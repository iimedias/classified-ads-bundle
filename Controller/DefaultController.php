<?php

namespace IiMedias\ClassifiedAdsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('IiMediasClassifiedAdsBundle:Default:index.html.twig');
    }
}
