<?php

namespace IiMedias\ClassifiedAdsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class NodeJsGetConfigCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nodejs:classifiedads:getconfig')
            ->setDescription('Récupère la configuration du ClassifiedAds pour le serveur nodejs')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = array();

        $output->write(json_encode($config));
    }

}
