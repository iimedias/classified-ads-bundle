<?php

namespace IiMedias\ClassifiedAdsBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds as ChildClassifiedAds;
use IiMedias\ClassifiedAdsBundle\Model\ClassifiedAdsQuery as ChildClassifiedAdsQuery;
use IiMedias\ClassifiedAdsBundle\Model\Map\ClassifiedAdsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'classifiedads_classifieds_ads_cacads' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.ClassifiedAdsBundle.Model.Base
 */
abstract class ClassifiedAds implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\ClassifiedAdsBundle\\Model\\Map\\ClassifiedAdsTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the cacads_id field.
     *
     * @var        int
     */
    protected $cacads_id;

    /**
     * The value for the cacads_usrusr_id field.
     *
     * @var        int
     */
    protected $cacads_usrusr_id;

    /**
     * The value for the cacads_cacatg_id field.
     *
     * @var        int
     */
    protected $cacads_cacatg_id;

    /**
     * The value for the cacads_request field.
     *
     * @var        int
     */
    protected $cacads_request;

    /**
     * The value for the cacads_type field.
     *
     * @var        int
     */
    protected $cacads_type;

    /**
     * The value for the cacads_name field.
     *
     * @var        string
     */
    protected $cacads_name;

    /**
     * The value for the cacads_description field.
     *
     * @var        string
     */
    protected $cacads_description;

    /**
     * The value for the cacads_price field.
     *
     * @var        double
     */
    protected $cacads_price;

    /**
     * The value for the cacads_photo_list field.
     *
     * @var        array
     */
    protected $cacads_photo_list;

    /**
     * The unserialized $cacads_photo_list value - i.e. the persisted object.
     * This is necessary to avoid repeated calls to unserialize() at runtime.
     * @var object
     */
    protected $cacads_photo_list_unserialized;

    /**
     * The value for the cacads_adtown_id field.
     *
     * @var        string
     */
    protected $cacads_adtown_id;

    /**
     * The value for the cacads_adstrt_id field.
     *
     * @var        string
     */
    protected $cacads_adstrt_id;

    /**
     * The value for the cacads_adhsnb_id field.
     *
     * @var        int
     */
    protected $cacads_adhsnb_id;

    /**
     * The value for the cacads_is_urgent field.
     *
     * @var        boolean
     */
    protected $cacads_is_urgent;

    /**
     * The value for the cacads_is_valid field.
     *
     * @var        boolean
     */
    protected $cacads_is_valid;

    /**
     * The value for the cacads_is_photo_pack field.
     *
     * @var        boolean
     */
    protected $cacads_is_photo_pack;

    /**
     * The value for the cacads_is_repost_pack field.
     *
     * @var        boolean
     */
    protected $cacads_is_repost_pack;

    /**
     * The value for the cacads_is_top_pack field.
     *
     * @var        boolean
     */
    protected $cacads_is_top_pack;

    /**
     * The value for the cacads_repost_limit field.
     *
     * @var        DateTime
     */
    protected $cacads_repost_limit;

    /**
     * The value for the cacads_repost_hour field.
     *
     * @var        DateTime
     */
    protected $cacads_repost_hour;

    /**
     * The value for the cacads_posted_at field.
     *
     * @var        DateTime
     */
    protected $cacads_posted_at;

    /**
     * The value for the cacads_created_at field.
     *
     * @var        DateTime
     */
    protected $cacads_created_at;

    /**
     * The value for the cacads_updated_at field.
     *
     * @var        DateTime
     */
    protected $cacads_updated_at;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of IiMedias\ClassifiedAdsBundle\Model\Base\ClassifiedAds object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ClassifiedAds</code> instance.  If
     * <code>obj</code> is an instance of <code>ClassifiedAds</code>, delegates to
     * <code>equals(ClassifiedAds)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|ClassifiedAds The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [cacads_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->cacads_id;
    }

    /**
     * Get the [cacads_usrusr_id] column value.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->cacads_usrusr_id;
    }

    /**
     * Get the [cacads_cacatg_id] column value.
     *
     * @return int
     */
    public function getCategoryId()
    {
        return $this->cacads_cacatg_id;
    }

    /**
     * Get the [cacads_request] column value.
     *
     * @return string
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getRequest()
    {
        if (null === $this->cacads_request) {
            return null;
        }
        $valueSet = ClassifiedAdsTableMap::getValueSet(ClassifiedAdsTableMap::COL_CACADS_REQUEST);
        if (!isset($valueSet[$this->cacads_request])) {
            throw new PropelException('Unknown stored enum key: ' . $this->cacads_request);
        }

        return $valueSet[$this->cacads_request];
    }

    /**
     * Get the [cacads_type] column value.
     *
     * @return string
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getType()
    {
        if (null === $this->cacads_type) {
            return null;
        }
        $valueSet = ClassifiedAdsTableMap::getValueSet(ClassifiedAdsTableMap::COL_CACADS_TYPE);
        if (!isset($valueSet[$this->cacads_type])) {
            throw new PropelException('Unknown stored enum key: ' . $this->cacads_type);
        }

        return $valueSet[$this->cacads_type];
    }

    /**
     * Get the [cacads_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->cacads_name;
    }

    /**
     * Get the [cacads_description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->cacads_description;
    }

    /**
     * Get the [cacads_price] column value.
     *
     * @return double
     */
    public function getPrice()
    {
        return $this->cacads_price;
    }

    /**
     * Get the [cacads_photo_list] column value.
     *
     * @return array
     */
    public function getPhotoList()
    {
        if (null === $this->cacads_photo_list_unserialized) {
            $this->cacads_photo_list_unserialized = array();
        }
        if (!$this->cacads_photo_list_unserialized && null !== $this->cacads_photo_list) {
            $cacads_photo_list_unserialized = substr($this->cacads_photo_list, 2, -2);
            $this->cacads_photo_list_unserialized = $cacads_photo_list_unserialized ? explode(' | ', $cacads_photo_list_unserialized) : array();
        }

        return $this->cacads_photo_list_unserialized;
    }

    /**
     * Get the [cacads_adtown_id] column value.
     *
     * @return string
     */
    public function getTownId()
    {
        return $this->cacads_adtown_id;
    }

    /**
     * Get the [cacads_adstrt_id] column value.
     *
     * @return string
     */
    public function getStreetId()
    {
        return $this->cacads_adstrt_id;
    }

    /**
     * Get the [cacads_adhsnb_id] column value.
     *
     * @return int
     */
    public function getHouseNumberId()
    {
        return $this->cacads_adhsnb_id;
    }

    /**
     * Get the [cacads_is_urgent] column value.
     *
     * @return boolean
     */
    public function getIsUrgent()
    {
        return $this->cacads_is_urgent;
    }

    /**
     * Get the [cacads_is_urgent] column value.
     *
     * @return boolean
     */
    public function isUrgent()
    {
        return $this->getIsUrgent();
    }

    /**
     * Get the [cacads_is_valid] column value.
     *
     * @return boolean
     */
    public function getIsValid()
    {
        return $this->cacads_is_valid;
    }

    /**
     * Get the [cacads_is_valid] column value.
     *
     * @return boolean
     */
    public function isValid()
    {
        return $this->getIsValid();
    }

    /**
     * Get the [cacads_is_photo_pack] column value.
     *
     * @return boolean
     */
    public function getIsPhotoPack()
    {
        return $this->cacads_is_photo_pack;
    }

    /**
     * Get the [cacads_is_photo_pack] column value.
     *
     * @return boolean
     */
    public function isPhotoPack()
    {
        return $this->getIsPhotoPack();
    }

    /**
     * Get the [cacads_is_repost_pack] column value.
     *
     * @return boolean
     */
    public function getIsRepostPack()
    {
        return $this->cacads_is_repost_pack;
    }

    /**
     * Get the [cacads_is_repost_pack] column value.
     *
     * @return boolean
     */
    public function isRepostPack()
    {
        return $this->getIsRepostPack();
    }

    /**
     * Get the [cacads_is_top_pack] column value.
     *
     * @return boolean
     */
    public function getIsTopPack()
    {
        return $this->cacads_is_top_pack;
    }

    /**
     * Get the [cacads_is_top_pack] column value.
     *
     * @return boolean
     */
    public function isTopPack()
    {
        return $this->getIsTopPack();
    }

    /**
     * Get the [optionally formatted] temporal [cacads_repost_limit] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getRepostLimit($format = NULL)
    {
        if ($format === null) {
            return $this->cacads_repost_limit;
        } else {
            return $this->cacads_repost_limit instanceof \DateTimeInterface ? $this->cacads_repost_limit->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [cacads_repost_hour] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getRepostHour($format = NULL)
    {
        if ($format === null) {
            return $this->cacads_repost_hour;
        } else {
            return $this->cacads_repost_hour instanceof \DateTimeInterface ? $this->cacads_repost_hour->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [cacads_posted_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getPostedAt($format = NULL)
    {
        if ($format === null) {
            return $this->cacads_posted_at;
        } else {
            return $this->cacads_posted_at instanceof \DateTimeInterface ? $this->cacads_posted_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [cacads_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->cacads_created_at;
        } else {
            return $this->cacads_created_at instanceof \DateTimeInterface ? $this->cacads_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [cacads_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->cacads_updated_at;
        } else {
            return $this->cacads_updated_at instanceof \DateTimeInterface ? $this->cacads_updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [cacads_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->cacads_id !== $v) {
            $this->cacads_id = $v;
            $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [cacads_usrusr_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     */
    public function setUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->cacads_usrusr_id !== $v) {
            $this->cacads_usrusr_id = $v;
            $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_USRUSR_ID] = true;
        }

        return $this;
    } // setUserId()

    /**
     * Set the value of [cacads_cacatg_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     */
    public function setCategoryId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->cacads_cacatg_id !== $v) {
            $this->cacads_cacatg_id = $v;
            $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_CACATG_ID] = true;
        }

        return $this;
    } // setCategoryId()

    /**
     * Set the value of [cacads_request] column.
     *
     * @param  string $v new value
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setRequest($v)
    {
        if ($v !== null) {
            $valueSet = ClassifiedAdsTableMap::getValueSet(ClassifiedAdsTableMap::COL_CACADS_REQUEST);
            if (!in_array($v, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $v));
            }
            $v = array_search($v, $valueSet);
        }

        if ($this->cacads_request !== $v) {
            $this->cacads_request = $v;
            $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_REQUEST] = true;
        }

        return $this;
    } // setRequest()

    /**
     * Set the value of [cacads_type] column.
     *
     * @param  string $v new value
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setType($v)
    {
        if ($v !== null) {
            $valueSet = ClassifiedAdsTableMap::getValueSet(ClassifiedAdsTableMap::COL_CACADS_TYPE);
            if (!in_array($v, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $v));
            }
            $v = array_search($v, $valueSet);
        }

        if ($this->cacads_type !== $v) {
            $this->cacads_type = $v;
            $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_TYPE] = true;
        }

        return $this;
    } // setType()

    /**
     * Set the value of [cacads_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cacads_name !== $v) {
            $this->cacads_name = $v;
            $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [cacads_description] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cacads_description !== $v) {
            $this->cacads_description = $v;
            $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [cacads_price] column.
     *
     * @param double $v new value
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     */
    public function setPrice($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->cacads_price !== $v) {
            $this->cacads_price = $v;
            $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_PRICE] = true;
        }

        return $this;
    } // setPrice()

    /**
     * Set the value of [cacads_photo_list] column.
     *
     * @param array $v new value
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     */
    public function setPhotoList($v)
    {
        if ($this->cacads_photo_list_unserialized !== $v) {
            $this->cacads_photo_list_unserialized = $v;
            $this->cacads_photo_list = '| ' . implode(' | ', $v) . ' |';
            $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_PHOTO_LIST] = true;
        }

        return $this;
    } // setPhotoList()

    /**
     * Set the value of [cacads_adtown_id] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     */
    public function setTownId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cacads_adtown_id !== $v) {
            $this->cacads_adtown_id = $v;
            $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_ADTOWN_ID] = true;
        }

        return $this;
    } // setTownId()

    /**
     * Set the value of [cacads_adstrt_id] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     */
    public function setStreetId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cacads_adstrt_id !== $v) {
            $this->cacads_adstrt_id = $v;
            $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_ADSTRT_ID] = true;
        }

        return $this;
    } // setStreetId()

    /**
     * Set the value of [cacads_adhsnb_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     */
    public function setHouseNumberId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->cacads_adhsnb_id !== $v) {
            $this->cacads_adhsnb_id = $v;
            $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_ADHSNB_ID] = true;
        }

        return $this;
    } // setHouseNumberId()

    /**
     * Sets the value of the [cacads_is_urgent] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     */
    public function setIsUrgent($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->cacads_is_urgent !== $v) {
            $this->cacads_is_urgent = $v;
            $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_IS_URGENT] = true;
        }

        return $this;
    } // setIsUrgent()

    /**
     * Sets the value of the [cacads_is_valid] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     */
    public function setIsValid($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->cacads_is_valid !== $v) {
            $this->cacads_is_valid = $v;
            $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_IS_VALID] = true;
        }

        return $this;
    } // setIsValid()

    /**
     * Sets the value of the [cacads_is_photo_pack] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     */
    public function setIsPhotoPack($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->cacads_is_photo_pack !== $v) {
            $this->cacads_is_photo_pack = $v;
            $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_IS_PHOTO_PACK] = true;
        }

        return $this;
    } // setIsPhotoPack()

    /**
     * Sets the value of the [cacads_is_repost_pack] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     */
    public function setIsRepostPack($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->cacads_is_repost_pack !== $v) {
            $this->cacads_is_repost_pack = $v;
            $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_IS_REPOST_PACK] = true;
        }

        return $this;
    } // setIsRepostPack()

    /**
     * Sets the value of the [cacads_is_top_pack] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     */
    public function setIsTopPack($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->cacads_is_top_pack !== $v) {
            $this->cacads_is_top_pack = $v;
            $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_IS_TOP_PACK] = true;
        }

        return $this;
    } // setIsTopPack()

    /**
     * Sets the value of [cacads_repost_limit] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     */
    public function setRepostLimit($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->cacads_repost_limit !== null || $dt !== null) {
            if ($this->cacads_repost_limit === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->cacads_repost_limit->format("Y-m-d H:i:s.u")) {
                $this->cacads_repost_limit = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_REPOST_LIMIT] = true;
            }
        } // if either are not null

        return $this;
    } // setRepostLimit()

    /**
     * Sets the value of [cacads_repost_hour] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     */
    public function setRepostHour($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->cacads_repost_hour !== null || $dt !== null) {
            if ($this->cacads_repost_hour === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->cacads_repost_hour->format("Y-m-d H:i:s.u")) {
                $this->cacads_repost_hour = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_REPOST_HOUR] = true;
            }
        } // if either are not null

        return $this;
    } // setRepostHour()

    /**
     * Sets the value of [cacads_posted_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     */
    public function setPostedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->cacads_posted_at !== null || $dt !== null) {
            if ($this->cacads_posted_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->cacads_posted_at->format("Y-m-d H:i:s.u")) {
                $this->cacads_posted_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_POSTED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setPostedAt()

    /**
     * Sets the value of [cacads_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->cacads_created_at !== null || $dt !== null) {
            if ($this->cacads_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->cacads_created_at->format("Y-m-d H:i:s.u")) {
                $this->cacads_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [cacads_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->cacads_updated_at !== null || $dt !== null) {
            if ($this->cacads_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->cacads_updated_at->format("Y-m-d H:i:s.u")) {
                $this->cacads_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ClassifiedAdsTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cacads_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ClassifiedAdsTableMap::translateFieldName('UserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cacads_usrusr_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ClassifiedAdsTableMap::translateFieldName('CategoryId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cacads_cacatg_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ClassifiedAdsTableMap::translateFieldName('Request', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cacads_request = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ClassifiedAdsTableMap::translateFieldName('Type', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cacads_type = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ClassifiedAdsTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cacads_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ClassifiedAdsTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cacads_description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ClassifiedAdsTableMap::translateFieldName('Price', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cacads_price = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ClassifiedAdsTableMap::translateFieldName('PhotoList', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cacads_photo_list = $col;
            $this->cacads_photo_list_unserialized = null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ClassifiedAdsTableMap::translateFieldName('TownId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cacads_adtown_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ClassifiedAdsTableMap::translateFieldName('StreetId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cacads_adstrt_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ClassifiedAdsTableMap::translateFieldName('HouseNumberId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cacads_adhsnb_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ClassifiedAdsTableMap::translateFieldName('IsUrgent', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cacads_is_urgent = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ClassifiedAdsTableMap::translateFieldName('IsValid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cacads_is_valid = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ClassifiedAdsTableMap::translateFieldName('IsPhotoPack', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cacads_is_photo_pack = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ClassifiedAdsTableMap::translateFieldName('IsRepostPack', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cacads_is_repost_pack = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : ClassifiedAdsTableMap::translateFieldName('IsTopPack', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cacads_is_top_pack = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : ClassifiedAdsTableMap::translateFieldName('RepostLimit', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->cacads_repost_limit = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : ClassifiedAdsTableMap::translateFieldName('RepostHour', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->cacads_repost_hour = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : ClassifiedAdsTableMap::translateFieldName('PostedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->cacads_posted_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : ClassifiedAdsTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->cacads_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : ClassifiedAdsTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->cacads_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 22; // 22 = ClassifiedAdsTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\ClassifiedAdsBundle\\Model\\ClassifiedAds'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ClassifiedAdsTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildClassifiedAdsQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ClassifiedAds::setDeleted()
     * @see ClassifiedAds::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClassifiedAdsTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildClassifiedAdsQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClassifiedAdsTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_CREATED_AT)) {
                    $this->setCreatedAt(time());
                }
                if (!$this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ClassifiedAdsTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_ID] = true;
        if (null !== $this->cacads_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ClassifiedAdsTableMap::COL_CACADS_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_ID)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_id';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_USRUSR_ID)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_usrusr_id';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_CACATG_ID)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_cacatg_id';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_REQUEST)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_request';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_type';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_name';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_description';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_PRICE)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_price';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_PHOTO_LIST)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_photo_list';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_ADTOWN_ID)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_adtown_id';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_ADSTRT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_adstrt_id';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_ADHSNB_ID)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_adhsnb_id';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_IS_URGENT)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_is_urgent';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_IS_VALID)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_is_valid';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_IS_PHOTO_PACK)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_is_photo_pack';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_IS_REPOST_PACK)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_is_repost_pack';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_IS_TOP_PACK)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_is_top_pack';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_REPOST_LIMIT)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_repost_limit';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_REPOST_HOUR)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_repost_hour';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_POSTED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_posted_at';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_created_at';
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'cacads_updated_at';
        }

        $sql = sprintf(
            'INSERT INTO classifiedads_classifieds_ads_cacads (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'cacads_id':
                        $stmt->bindValue($identifier, $this->cacads_id, PDO::PARAM_INT);
                        break;
                    case 'cacads_usrusr_id':
                        $stmt->bindValue($identifier, $this->cacads_usrusr_id, PDO::PARAM_INT);
                        break;
                    case 'cacads_cacatg_id':
                        $stmt->bindValue($identifier, $this->cacads_cacatg_id, PDO::PARAM_INT);
                        break;
                    case 'cacads_request':
                        $stmt->bindValue($identifier, $this->cacads_request, PDO::PARAM_INT);
                        break;
                    case 'cacads_type':
                        $stmt->bindValue($identifier, $this->cacads_type, PDO::PARAM_INT);
                        break;
                    case 'cacads_name':
                        $stmt->bindValue($identifier, $this->cacads_name, PDO::PARAM_STR);
                        break;
                    case 'cacads_description':
                        $stmt->bindValue($identifier, $this->cacads_description, PDO::PARAM_STR);
                        break;
                    case 'cacads_price':
                        $stmt->bindValue($identifier, $this->cacads_price, PDO::PARAM_STR);
                        break;
                    case 'cacads_photo_list':
                        $stmt->bindValue($identifier, $this->cacads_photo_list, PDO::PARAM_STR);
                        break;
                    case 'cacads_adtown_id':
                        $stmt->bindValue($identifier, $this->cacads_adtown_id, PDO::PARAM_STR);
                        break;
                    case 'cacads_adstrt_id':
                        $stmt->bindValue($identifier, $this->cacads_adstrt_id, PDO::PARAM_STR);
                        break;
                    case 'cacads_adhsnb_id':
                        $stmt->bindValue($identifier, $this->cacads_adhsnb_id, PDO::PARAM_INT);
                        break;
                    case 'cacads_is_urgent':
                        $stmt->bindValue($identifier, (int) $this->cacads_is_urgent, PDO::PARAM_INT);
                        break;
                    case 'cacads_is_valid':
                        $stmt->bindValue($identifier, (int) $this->cacads_is_valid, PDO::PARAM_INT);
                        break;
                    case 'cacads_is_photo_pack':
                        $stmt->bindValue($identifier, (int) $this->cacads_is_photo_pack, PDO::PARAM_INT);
                        break;
                    case 'cacads_is_repost_pack':
                        $stmt->bindValue($identifier, (int) $this->cacads_is_repost_pack, PDO::PARAM_INT);
                        break;
                    case 'cacads_is_top_pack':
                        $stmt->bindValue($identifier, (int) $this->cacads_is_top_pack, PDO::PARAM_INT);
                        break;
                    case 'cacads_repost_limit':
                        $stmt->bindValue($identifier, $this->cacads_repost_limit ? $this->cacads_repost_limit->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'cacads_repost_hour':
                        $stmt->bindValue($identifier, $this->cacads_repost_hour ? $this->cacads_repost_hour->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'cacads_posted_at':
                        $stmt->bindValue($identifier, $this->cacads_posted_at ? $this->cacads_posted_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'cacads_created_at':
                        $stmt->bindValue($identifier, $this->cacads_created_at ? $this->cacads_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'cacads_updated_at':
                        $stmt->bindValue($identifier, $this->cacads_updated_at ? $this->cacads_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ClassifiedAdsTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getUserId();
                break;
            case 2:
                return $this->getCategoryId();
                break;
            case 3:
                return $this->getRequest();
                break;
            case 4:
                return $this->getType();
                break;
            case 5:
                return $this->getName();
                break;
            case 6:
                return $this->getDescription();
                break;
            case 7:
                return $this->getPrice();
                break;
            case 8:
                return $this->getPhotoList();
                break;
            case 9:
                return $this->getTownId();
                break;
            case 10:
                return $this->getStreetId();
                break;
            case 11:
                return $this->getHouseNumberId();
                break;
            case 12:
                return $this->getIsUrgent();
                break;
            case 13:
                return $this->getIsValid();
                break;
            case 14:
                return $this->getIsPhotoPack();
                break;
            case 15:
                return $this->getIsRepostPack();
                break;
            case 16:
                return $this->getIsTopPack();
                break;
            case 17:
                return $this->getRepostLimit();
                break;
            case 18:
                return $this->getRepostHour();
                break;
            case 19:
                return $this->getPostedAt();
                break;
            case 20:
                return $this->getCreatedAt();
                break;
            case 21:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['ClassifiedAds'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ClassifiedAds'][$this->hashCode()] = true;
        $keys = ClassifiedAdsTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getUserId(),
            $keys[2] => $this->getCategoryId(),
            $keys[3] => $this->getRequest(),
            $keys[4] => $this->getType(),
            $keys[5] => $this->getName(),
            $keys[6] => $this->getDescription(),
            $keys[7] => $this->getPrice(),
            $keys[8] => $this->getPhotoList(),
            $keys[9] => $this->getTownId(),
            $keys[10] => $this->getStreetId(),
            $keys[11] => $this->getHouseNumberId(),
            $keys[12] => $this->getIsUrgent(),
            $keys[13] => $this->getIsValid(),
            $keys[14] => $this->getIsPhotoPack(),
            $keys[15] => $this->getIsRepostPack(),
            $keys[16] => $this->getIsTopPack(),
            $keys[17] => $this->getRepostLimit(),
            $keys[18] => $this->getRepostHour(),
            $keys[19] => $this->getPostedAt(),
            $keys[20] => $this->getCreatedAt(),
            $keys[21] => $this->getUpdatedAt(),
        );
        if ($result[$keys[17]] instanceof \DateTime) {
            $result[$keys[17]] = $result[$keys[17]]->format('c');
        }

        if ($result[$keys[18]] instanceof \DateTime) {
            $result[$keys[18]] = $result[$keys[18]]->format('c');
        }

        if ($result[$keys[19]] instanceof \DateTime) {
            $result[$keys[19]] = $result[$keys[19]]->format('c');
        }

        if ($result[$keys[20]] instanceof \DateTime) {
            $result[$keys[20]] = $result[$keys[20]]->format('c');
        }

        if ($result[$keys[21]] instanceof \DateTime) {
            $result[$keys[21]] = $result[$keys[21]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ClassifiedAdsTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setUserId($value);
                break;
            case 2:
                $this->setCategoryId($value);
                break;
            case 3:
                $valueSet = ClassifiedAdsTableMap::getValueSet(ClassifiedAdsTableMap::COL_CACADS_REQUEST);
                if (isset($valueSet[$value])) {
                    $value = $valueSet[$value];
                }
                $this->setRequest($value);
                break;
            case 4:
                $valueSet = ClassifiedAdsTableMap::getValueSet(ClassifiedAdsTableMap::COL_CACADS_TYPE);
                if (isset($valueSet[$value])) {
                    $value = $valueSet[$value];
                }
                $this->setType($value);
                break;
            case 5:
                $this->setName($value);
                break;
            case 6:
                $this->setDescription($value);
                break;
            case 7:
                $this->setPrice($value);
                break;
            case 8:
                if (!is_array($value)) {
                    $v = trim(substr($value, 2, -2));
                    $value = $v ? explode(' | ', $v) : array();
                }
                $this->setPhotoList($value);
                break;
            case 9:
                $this->setTownId($value);
                break;
            case 10:
                $this->setStreetId($value);
                break;
            case 11:
                $this->setHouseNumberId($value);
                break;
            case 12:
                $this->setIsUrgent($value);
                break;
            case 13:
                $this->setIsValid($value);
                break;
            case 14:
                $this->setIsPhotoPack($value);
                break;
            case 15:
                $this->setIsRepostPack($value);
                break;
            case 16:
                $this->setIsTopPack($value);
                break;
            case 17:
                $this->setRepostLimit($value);
                break;
            case 18:
                $this->setRepostHour($value);
                break;
            case 19:
                $this->setPostedAt($value);
                break;
            case 20:
                $this->setCreatedAt($value);
                break;
            case 21:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ClassifiedAdsTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setUserId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setCategoryId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setRequest($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setType($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setName($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setDescription($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setPrice($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setPhotoList($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setTownId($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setStreetId($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setHouseNumberId($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setIsUrgent($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setIsValid($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setIsPhotoPack($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setIsRepostPack($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setIsTopPack($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setRepostLimit($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setRepostHour($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setPostedAt($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setCreatedAt($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setUpdatedAt($arr[$keys[21]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ClassifiedAdsTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_ID)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_ID, $this->cacads_id);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_USRUSR_ID)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_USRUSR_ID, $this->cacads_usrusr_id);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_CACATG_ID)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_CACATG_ID, $this->cacads_cacatg_id);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_REQUEST)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_REQUEST, $this->cacads_request);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_TYPE)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_TYPE, $this->cacads_type);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_NAME)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_NAME, $this->cacads_name);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_DESCRIPTION)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_DESCRIPTION, $this->cacads_description);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_PRICE)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_PRICE, $this->cacads_price);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_PHOTO_LIST)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_PHOTO_LIST, $this->cacads_photo_list);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_ADTOWN_ID)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_ADTOWN_ID, $this->cacads_adtown_id);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_ADSTRT_ID)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_ADSTRT_ID, $this->cacads_adstrt_id);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_ADHSNB_ID)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_ADHSNB_ID, $this->cacads_adhsnb_id);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_IS_URGENT)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_IS_URGENT, $this->cacads_is_urgent);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_IS_VALID)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_IS_VALID, $this->cacads_is_valid);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_IS_PHOTO_PACK)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_IS_PHOTO_PACK, $this->cacads_is_photo_pack);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_IS_REPOST_PACK)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_IS_REPOST_PACK, $this->cacads_is_repost_pack);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_IS_TOP_PACK)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_IS_TOP_PACK, $this->cacads_is_top_pack);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_REPOST_LIMIT)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_REPOST_LIMIT, $this->cacads_repost_limit);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_REPOST_HOUR)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_REPOST_HOUR, $this->cacads_repost_hour);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_POSTED_AT)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_POSTED_AT, $this->cacads_posted_at);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_CREATED_AT)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_CREATED_AT, $this->cacads_created_at);
        }
        if ($this->isColumnModified(ClassifiedAdsTableMap::COL_CACADS_UPDATED_AT)) {
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_UPDATED_AT, $this->cacads_updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildClassifiedAdsQuery::create();
        $criteria->add(ClassifiedAdsTableMap::COL_CACADS_ID, $this->cacads_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (cacads_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUserId($this->getUserId());
        $copyObj->setCategoryId($this->getCategoryId());
        $copyObj->setRequest($this->getRequest());
        $copyObj->setType($this->getType());
        $copyObj->setName($this->getName());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setPrice($this->getPrice());
        $copyObj->setPhotoList($this->getPhotoList());
        $copyObj->setTownId($this->getTownId());
        $copyObj->setStreetId($this->getStreetId());
        $copyObj->setHouseNumberId($this->getHouseNumberId());
        $copyObj->setIsUrgent($this->getIsUrgent());
        $copyObj->setIsValid($this->getIsValid());
        $copyObj->setIsPhotoPack($this->getIsPhotoPack());
        $copyObj->setIsRepostPack($this->getIsRepostPack());
        $copyObj->setIsTopPack($this->getIsTopPack());
        $copyObj->setRepostLimit($this->getRepostLimit());
        $copyObj->setRepostHour($this->getRepostHour());
        $copyObj->setPostedAt($this->getPostedAt());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->cacads_id = null;
        $this->cacads_usrusr_id = null;
        $this->cacads_cacatg_id = null;
        $this->cacads_request = null;
        $this->cacads_type = null;
        $this->cacads_name = null;
        $this->cacads_description = null;
        $this->cacads_price = null;
        $this->cacads_photo_list = null;
        $this->cacads_photo_list_unserialized = null;
        $this->cacads_adtown_id = null;
        $this->cacads_adstrt_id = null;
        $this->cacads_adhsnb_id = null;
        $this->cacads_is_urgent = null;
        $this->cacads_is_valid = null;
        $this->cacads_is_photo_pack = null;
        $this->cacads_is_repost_pack = null;
        $this->cacads_is_top_pack = null;
        $this->cacads_repost_limit = null;
        $this->cacads_repost_hour = null;
        $this->cacads_posted_at = null;
        $this->cacads_created_at = null;
        $this->cacads_updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ClassifiedAdsTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildClassifiedAds The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[ClassifiedAdsTableMap::COL_CACADS_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
