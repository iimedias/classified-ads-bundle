<?php

namespace IiMedias\ClassifiedAdsBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds as ChildClassifiedAds;
use IiMedias\ClassifiedAdsBundle\Model\ClassifiedAdsQuery as ChildClassifiedAdsQuery;
use IiMedias\ClassifiedAdsBundle\Model\Map\ClassifiedAdsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'classifiedads_classifieds_ads_cacads' table.
 *
 *
 *
 * @method     ChildClassifiedAdsQuery orderById($order = Criteria::ASC) Order by the cacads_id column
 * @method     ChildClassifiedAdsQuery orderByUserId($order = Criteria::ASC) Order by the cacads_usrusr_id column
 * @method     ChildClassifiedAdsQuery orderByCategoryId($order = Criteria::ASC) Order by the cacads_cacatg_id column
 * @method     ChildClassifiedAdsQuery orderByRequest($order = Criteria::ASC) Order by the cacads_request column
 * @method     ChildClassifiedAdsQuery orderByType($order = Criteria::ASC) Order by the cacads_type column
 * @method     ChildClassifiedAdsQuery orderByName($order = Criteria::ASC) Order by the cacads_name column
 * @method     ChildClassifiedAdsQuery orderByDescription($order = Criteria::ASC) Order by the cacads_description column
 * @method     ChildClassifiedAdsQuery orderByPrice($order = Criteria::ASC) Order by the cacads_price column
 * @method     ChildClassifiedAdsQuery orderByPhotoList($order = Criteria::ASC) Order by the cacads_photo_list column
 * @method     ChildClassifiedAdsQuery orderByTownId($order = Criteria::ASC) Order by the cacads_adtown_id column
 * @method     ChildClassifiedAdsQuery orderByStreetId($order = Criteria::ASC) Order by the cacads_adstrt_id column
 * @method     ChildClassifiedAdsQuery orderByHouseNumberId($order = Criteria::ASC) Order by the cacads_adhsnb_id column
 * @method     ChildClassifiedAdsQuery orderByIsUrgent($order = Criteria::ASC) Order by the cacads_is_urgent column
 * @method     ChildClassifiedAdsQuery orderByIsValid($order = Criteria::ASC) Order by the cacads_is_valid column
 * @method     ChildClassifiedAdsQuery orderByIsPhotoPack($order = Criteria::ASC) Order by the cacads_is_photo_pack column
 * @method     ChildClassifiedAdsQuery orderByIsRepostPack($order = Criteria::ASC) Order by the cacads_is_repost_pack column
 * @method     ChildClassifiedAdsQuery orderByIsTopPack($order = Criteria::ASC) Order by the cacads_is_top_pack column
 * @method     ChildClassifiedAdsQuery orderByRepostLimit($order = Criteria::ASC) Order by the cacads_repost_limit column
 * @method     ChildClassifiedAdsQuery orderByRepostHour($order = Criteria::ASC) Order by the cacads_repost_hour column
 * @method     ChildClassifiedAdsQuery orderByPostedAt($order = Criteria::ASC) Order by the cacads_posted_at column
 * @method     ChildClassifiedAdsQuery orderByCreatedAt($order = Criteria::ASC) Order by the cacads_created_at column
 * @method     ChildClassifiedAdsQuery orderByUpdatedAt($order = Criteria::ASC) Order by the cacads_updated_at column
 *
 * @method     ChildClassifiedAdsQuery groupById() Group by the cacads_id column
 * @method     ChildClassifiedAdsQuery groupByUserId() Group by the cacads_usrusr_id column
 * @method     ChildClassifiedAdsQuery groupByCategoryId() Group by the cacads_cacatg_id column
 * @method     ChildClassifiedAdsQuery groupByRequest() Group by the cacads_request column
 * @method     ChildClassifiedAdsQuery groupByType() Group by the cacads_type column
 * @method     ChildClassifiedAdsQuery groupByName() Group by the cacads_name column
 * @method     ChildClassifiedAdsQuery groupByDescription() Group by the cacads_description column
 * @method     ChildClassifiedAdsQuery groupByPrice() Group by the cacads_price column
 * @method     ChildClassifiedAdsQuery groupByPhotoList() Group by the cacads_photo_list column
 * @method     ChildClassifiedAdsQuery groupByTownId() Group by the cacads_adtown_id column
 * @method     ChildClassifiedAdsQuery groupByStreetId() Group by the cacads_adstrt_id column
 * @method     ChildClassifiedAdsQuery groupByHouseNumberId() Group by the cacads_adhsnb_id column
 * @method     ChildClassifiedAdsQuery groupByIsUrgent() Group by the cacads_is_urgent column
 * @method     ChildClassifiedAdsQuery groupByIsValid() Group by the cacads_is_valid column
 * @method     ChildClassifiedAdsQuery groupByIsPhotoPack() Group by the cacads_is_photo_pack column
 * @method     ChildClassifiedAdsQuery groupByIsRepostPack() Group by the cacads_is_repost_pack column
 * @method     ChildClassifiedAdsQuery groupByIsTopPack() Group by the cacads_is_top_pack column
 * @method     ChildClassifiedAdsQuery groupByRepostLimit() Group by the cacads_repost_limit column
 * @method     ChildClassifiedAdsQuery groupByRepostHour() Group by the cacads_repost_hour column
 * @method     ChildClassifiedAdsQuery groupByPostedAt() Group by the cacads_posted_at column
 * @method     ChildClassifiedAdsQuery groupByCreatedAt() Group by the cacads_created_at column
 * @method     ChildClassifiedAdsQuery groupByUpdatedAt() Group by the cacads_updated_at column
 *
 * @method     ChildClassifiedAdsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildClassifiedAdsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildClassifiedAdsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildClassifiedAdsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildClassifiedAdsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildClassifiedAdsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildClassifiedAds findOne(ConnectionInterface $con = null) Return the first ChildClassifiedAds matching the query
 * @method     ChildClassifiedAds findOneOrCreate(ConnectionInterface $con = null) Return the first ChildClassifiedAds matching the query, or a new ChildClassifiedAds object populated from the query conditions when no match is found
 *
 * @method     ChildClassifiedAds findOneById(int $cacads_id) Return the first ChildClassifiedAds filtered by the cacads_id column
 * @method     ChildClassifiedAds findOneByUserId(int $cacads_usrusr_id) Return the first ChildClassifiedAds filtered by the cacads_usrusr_id column
 * @method     ChildClassifiedAds findOneByCategoryId(int $cacads_cacatg_id) Return the first ChildClassifiedAds filtered by the cacads_cacatg_id column
 * @method     ChildClassifiedAds findOneByRequest(int $cacads_request) Return the first ChildClassifiedAds filtered by the cacads_request column
 * @method     ChildClassifiedAds findOneByType(int $cacads_type) Return the first ChildClassifiedAds filtered by the cacads_type column
 * @method     ChildClassifiedAds findOneByName(string $cacads_name) Return the first ChildClassifiedAds filtered by the cacads_name column
 * @method     ChildClassifiedAds findOneByDescription(string $cacads_description) Return the first ChildClassifiedAds filtered by the cacads_description column
 * @method     ChildClassifiedAds findOneByPrice(double $cacads_price) Return the first ChildClassifiedAds filtered by the cacads_price column
 * @method     ChildClassifiedAds findOneByPhotoList(array $cacads_photo_list) Return the first ChildClassifiedAds filtered by the cacads_photo_list column
 * @method     ChildClassifiedAds findOneByTownId(string $cacads_adtown_id) Return the first ChildClassifiedAds filtered by the cacads_adtown_id column
 * @method     ChildClassifiedAds findOneByStreetId(string $cacads_adstrt_id) Return the first ChildClassifiedAds filtered by the cacads_adstrt_id column
 * @method     ChildClassifiedAds findOneByHouseNumberId(int $cacads_adhsnb_id) Return the first ChildClassifiedAds filtered by the cacads_adhsnb_id column
 * @method     ChildClassifiedAds findOneByIsUrgent(boolean $cacads_is_urgent) Return the first ChildClassifiedAds filtered by the cacads_is_urgent column
 * @method     ChildClassifiedAds findOneByIsValid(boolean $cacads_is_valid) Return the first ChildClassifiedAds filtered by the cacads_is_valid column
 * @method     ChildClassifiedAds findOneByIsPhotoPack(boolean $cacads_is_photo_pack) Return the first ChildClassifiedAds filtered by the cacads_is_photo_pack column
 * @method     ChildClassifiedAds findOneByIsRepostPack(boolean $cacads_is_repost_pack) Return the first ChildClassifiedAds filtered by the cacads_is_repost_pack column
 * @method     ChildClassifiedAds findOneByIsTopPack(boolean $cacads_is_top_pack) Return the first ChildClassifiedAds filtered by the cacads_is_top_pack column
 * @method     ChildClassifiedAds findOneByRepostLimit(string $cacads_repost_limit) Return the first ChildClassifiedAds filtered by the cacads_repost_limit column
 * @method     ChildClassifiedAds findOneByRepostHour(string $cacads_repost_hour) Return the first ChildClassifiedAds filtered by the cacads_repost_hour column
 * @method     ChildClassifiedAds findOneByPostedAt(string $cacads_posted_at) Return the first ChildClassifiedAds filtered by the cacads_posted_at column
 * @method     ChildClassifiedAds findOneByCreatedAt(string $cacads_created_at) Return the first ChildClassifiedAds filtered by the cacads_created_at column
 * @method     ChildClassifiedAds findOneByUpdatedAt(string $cacads_updated_at) Return the first ChildClassifiedAds filtered by the cacads_updated_at column *

 * @method     ChildClassifiedAds requirePk($key, ConnectionInterface $con = null) Return the ChildClassifiedAds by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOne(ConnectionInterface $con = null) Return the first ChildClassifiedAds matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildClassifiedAds requireOneById(int $cacads_id) Return the first ChildClassifiedAds filtered by the cacads_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByUserId(int $cacads_usrusr_id) Return the first ChildClassifiedAds filtered by the cacads_usrusr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByCategoryId(int $cacads_cacatg_id) Return the first ChildClassifiedAds filtered by the cacads_cacatg_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByRequest(int $cacads_request) Return the first ChildClassifiedAds filtered by the cacads_request column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByType(int $cacads_type) Return the first ChildClassifiedAds filtered by the cacads_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByName(string $cacads_name) Return the first ChildClassifiedAds filtered by the cacads_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByDescription(string $cacads_description) Return the first ChildClassifiedAds filtered by the cacads_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByPrice(double $cacads_price) Return the first ChildClassifiedAds filtered by the cacads_price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByPhotoList(array $cacads_photo_list) Return the first ChildClassifiedAds filtered by the cacads_photo_list column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByTownId(string $cacads_adtown_id) Return the first ChildClassifiedAds filtered by the cacads_adtown_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByStreetId(string $cacads_adstrt_id) Return the first ChildClassifiedAds filtered by the cacads_adstrt_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByHouseNumberId(int $cacads_adhsnb_id) Return the first ChildClassifiedAds filtered by the cacads_adhsnb_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByIsUrgent(boolean $cacads_is_urgent) Return the first ChildClassifiedAds filtered by the cacads_is_urgent column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByIsValid(boolean $cacads_is_valid) Return the first ChildClassifiedAds filtered by the cacads_is_valid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByIsPhotoPack(boolean $cacads_is_photo_pack) Return the first ChildClassifiedAds filtered by the cacads_is_photo_pack column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByIsRepostPack(boolean $cacads_is_repost_pack) Return the first ChildClassifiedAds filtered by the cacads_is_repost_pack column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByIsTopPack(boolean $cacads_is_top_pack) Return the first ChildClassifiedAds filtered by the cacads_is_top_pack column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByRepostLimit(string $cacads_repost_limit) Return the first ChildClassifiedAds filtered by the cacads_repost_limit column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByRepostHour(string $cacads_repost_hour) Return the first ChildClassifiedAds filtered by the cacads_repost_hour column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByPostedAt(string $cacads_posted_at) Return the first ChildClassifiedAds filtered by the cacads_posted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByCreatedAt(string $cacads_created_at) Return the first ChildClassifiedAds filtered by the cacads_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassifiedAds requireOneByUpdatedAt(string $cacads_updated_at) Return the first ChildClassifiedAds filtered by the cacads_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildClassifiedAds[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildClassifiedAds objects based on current ModelCriteria
 * @method     ChildClassifiedAds[]|ObjectCollection findById(int $cacads_id) Return ChildClassifiedAds objects filtered by the cacads_id column
 * @method     ChildClassifiedAds[]|ObjectCollection findByUserId(int $cacads_usrusr_id) Return ChildClassifiedAds objects filtered by the cacads_usrusr_id column
 * @method     ChildClassifiedAds[]|ObjectCollection findByCategoryId(int $cacads_cacatg_id) Return ChildClassifiedAds objects filtered by the cacads_cacatg_id column
 * @method     ChildClassifiedAds[]|ObjectCollection findByRequest(int $cacads_request) Return ChildClassifiedAds objects filtered by the cacads_request column
 * @method     ChildClassifiedAds[]|ObjectCollection findByType(int $cacads_type) Return ChildClassifiedAds objects filtered by the cacads_type column
 * @method     ChildClassifiedAds[]|ObjectCollection findByName(string $cacads_name) Return ChildClassifiedAds objects filtered by the cacads_name column
 * @method     ChildClassifiedAds[]|ObjectCollection findByDescription(string $cacads_description) Return ChildClassifiedAds objects filtered by the cacads_description column
 * @method     ChildClassifiedAds[]|ObjectCollection findByPrice(double $cacads_price) Return ChildClassifiedAds objects filtered by the cacads_price column
 * @method     ChildClassifiedAds[]|ObjectCollection findByPhotoList(array $cacads_photo_list) Return ChildClassifiedAds objects filtered by the cacads_photo_list column
 * @method     ChildClassifiedAds[]|ObjectCollection findByTownId(string $cacads_adtown_id) Return ChildClassifiedAds objects filtered by the cacads_adtown_id column
 * @method     ChildClassifiedAds[]|ObjectCollection findByStreetId(string $cacads_adstrt_id) Return ChildClassifiedAds objects filtered by the cacads_adstrt_id column
 * @method     ChildClassifiedAds[]|ObjectCollection findByHouseNumberId(int $cacads_adhsnb_id) Return ChildClassifiedAds objects filtered by the cacads_adhsnb_id column
 * @method     ChildClassifiedAds[]|ObjectCollection findByIsUrgent(boolean $cacads_is_urgent) Return ChildClassifiedAds objects filtered by the cacads_is_urgent column
 * @method     ChildClassifiedAds[]|ObjectCollection findByIsValid(boolean $cacads_is_valid) Return ChildClassifiedAds objects filtered by the cacads_is_valid column
 * @method     ChildClassifiedAds[]|ObjectCollection findByIsPhotoPack(boolean $cacads_is_photo_pack) Return ChildClassifiedAds objects filtered by the cacads_is_photo_pack column
 * @method     ChildClassifiedAds[]|ObjectCollection findByIsRepostPack(boolean $cacads_is_repost_pack) Return ChildClassifiedAds objects filtered by the cacads_is_repost_pack column
 * @method     ChildClassifiedAds[]|ObjectCollection findByIsTopPack(boolean $cacads_is_top_pack) Return ChildClassifiedAds objects filtered by the cacads_is_top_pack column
 * @method     ChildClassifiedAds[]|ObjectCollection findByRepostLimit(string $cacads_repost_limit) Return ChildClassifiedAds objects filtered by the cacads_repost_limit column
 * @method     ChildClassifiedAds[]|ObjectCollection findByRepostHour(string $cacads_repost_hour) Return ChildClassifiedAds objects filtered by the cacads_repost_hour column
 * @method     ChildClassifiedAds[]|ObjectCollection findByPostedAt(string $cacads_posted_at) Return ChildClassifiedAds objects filtered by the cacads_posted_at column
 * @method     ChildClassifiedAds[]|ObjectCollection findByCreatedAt(string $cacads_created_at) Return ChildClassifiedAds objects filtered by the cacads_created_at column
 * @method     ChildClassifiedAds[]|ObjectCollection findByUpdatedAt(string $cacads_updated_at) Return ChildClassifiedAds objects filtered by the cacads_updated_at column
 * @method     ChildClassifiedAds[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ClassifiedAdsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\ClassifiedAdsBundle\Model\Base\ClassifiedAdsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\ClassifiedAdsBundle\\Model\\ClassifiedAds', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildClassifiedAdsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildClassifiedAdsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildClassifiedAdsQuery) {
            return $criteria;
        }
        $query = new ChildClassifiedAdsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildClassifiedAds|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ClassifiedAdsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ClassifiedAdsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildClassifiedAds A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT cacads_id, cacads_usrusr_id, cacads_cacatg_id, cacads_request, cacads_type, cacads_name, cacads_description, cacads_price, cacads_photo_list, cacads_adtown_id, cacads_adstrt_id, cacads_adhsnb_id, cacads_is_urgent, cacads_is_valid, cacads_is_photo_pack, cacads_is_repost_pack, cacads_is_top_pack, cacads_repost_limit, cacads_repost_hour, cacads_posted_at, cacads_created_at, cacads_updated_at FROM classifiedads_classifieds_ads_cacads WHERE cacads_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildClassifiedAds $obj */
            $obj = new ChildClassifiedAds();
            $obj->hydrate($row);
            ClassifiedAdsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildClassifiedAds|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the cacads_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE cacads_id = 1234
     * $query->filterById(array(12, 34)); // WHERE cacads_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE cacads_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_ID, $id, $comparison);
    }

    /**
     * Filter the query on the cacads_usrusr_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE cacads_usrusr_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE cacads_usrusr_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE cacads_usrusr_id > 12
     * </code>
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_USRUSR_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_USRUSR_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_USRUSR_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the cacads_cacatg_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCategoryId(1234); // WHERE cacads_cacatg_id = 1234
     * $query->filterByCategoryId(array(12, 34)); // WHERE cacads_cacatg_id IN (12, 34)
     * $query->filterByCategoryId(array('min' => 12)); // WHERE cacads_cacatg_id > 12
     * </code>
     *
     * @param     mixed $categoryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByCategoryId($categoryId = null, $comparison = null)
    {
        if (is_array($categoryId)) {
            $useMinMax = false;
            if (isset($categoryId['min'])) {
                $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_CACATG_ID, $categoryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($categoryId['max'])) {
                $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_CACATG_ID, $categoryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_CACATG_ID, $categoryId, $comparison);
    }

    /**
     * Filter the query on the cacads_request column
     *
     * @param     mixed $request The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByRequest($request = null, $comparison = null)
    {
        $valueSet = ClassifiedAdsTableMap::getValueSet(ClassifiedAdsTableMap::COL_CACADS_REQUEST);
        if (is_scalar($request)) {
            if (!in_array($request, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $request));
            }
            $request = array_search($request, $valueSet);
        } elseif (is_array($request)) {
            $convertedValues = array();
            foreach ($request as $value) {
                if (!in_array($value, $valueSet)) {
                    throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $value));
                }
                $convertedValues []= array_search($value, $valueSet);
            }
            $request = $convertedValues;
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_REQUEST, $request, $comparison);
    }

    /**
     * Filter the query on the cacads_type column
     *
     * @param     mixed $type The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        $valueSet = ClassifiedAdsTableMap::getValueSet(ClassifiedAdsTableMap::COL_CACADS_TYPE);
        if (is_scalar($type)) {
            if (!in_array($type, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $type));
            }
            $type = array_search($type, $valueSet);
        } elseif (is_array($type)) {
            $convertedValues = array();
            foreach ($type as $value) {
                if (!in_array($value, $valueSet)) {
                    throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $value));
                }
                $convertedValues []= array_search($value, $valueSet);
            }
            $type = $convertedValues;
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the cacads_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE cacads_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE cacads_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the cacads_description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE cacads_description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE cacads_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the cacads_price column
     *
     * Example usage:
     * <code>
     * $query->filterByPrice(1234); // WHERE cacads_price = 1234
     * $query->filterByPrice(array(12, 34)); // WHERE cacads_price IN (12, 34)
     * $query->filterByPrice(array('min' => 12)); // WHERE cacads_price > 12
     * </code>
     *
     * @param     mixed $price The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByPrice($price = null, $comparison = null)
    {
        if (is_array($price)) {
            $useMinMax = false;
            if (isset($price['min'])) {
                $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_PRICE, $price['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($price['max'])) {
                $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_PRICE, $price['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_PRICE, $price, $comparison);
    }

    /**
     * Filter the query on the cacads_photo_list column
     *
     * @param     array $photoList The values to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByPhotoList($photoList = null, $comparison = null)
    {
        $key = $this->getAliasedColName(ClassifiedAdsTableMap::COL_CACADS_PHOTO_LIST);
        if (null === $comparison || $comparison == Criteria::CONTAINS_ALL) {
            foreach ($photoList as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_SOME) {
            foreach ($photoList as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addOr($key, $value, Criteria::LIKE);
                } else {
                    $this->add($key, $value, Criteria::LIKE);
                }
            }

            return $this;
        } elseif ($comparison == Criteria::CONTAINS_NONE) {
            foreach ($photoList as $value) {
                $value = '%| ' . $value . ' |%';
                if ($this->containsKey($key)) {
                    $this->addAnd($key, $value, Criteria::NOT_LIKE);
                } else {
                    $this->add($key, $value, Criteria::NOT_LIKE);
                }
            }
            $this->addOr($key, null, Criteria::ISNULL);

            return $this;
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_PHOTO_LIST, $photoList, $comparison);
    }

    /**
     * Filter the query on the cacads_adtown_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTownId('fooValue');   // WHERE cacads_adtown_id = 'fooValue'
     * $query->filterByTownId('%fooValue%'); // WHERE cacads_adtown_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $townId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByTownId($townId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($townId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_ADTOWN_ID, $townId, $comparison);
    }

    /**
     * Filter the query on the cacads_adstrt_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStreetId('fooValue');   // WHERE cacads_adstrt_id = 'fooValue'
     * $query->filterByStreetId('%fooValue%'); // WHERE cacads_adstrt_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $streetId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByStreetId($streetId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($streetId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_ADSTRT_ID, $streetId, $comparison);
    }

    /**
     * Filter the query on the cacads_adhsnb_id column
     *
     * Example usage:
     * <code>
     * $query->filterByHouseNumberId(1234); // WHERE cacads_adhsnb_id = 1234
     * $query->filterByHouseNumberId(array(12, 34)); // WHERE cacads_adhsnb_id IN (12, 34)
     * $query->filterByHouseNumberId(array('min' => 12)); // WHERE cacads_adhsnb_id > 12
     * </code>
     *
     * @param     mixed $houseNumberId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByHouseNumberId($houseNumberId = null, $comparison = null)
    {
        if (is_array($houseNumberId)) {
            $useMinMax = false;
            if (isset($houseNumberId['min'])) {
                $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_ADHSNB_ID, $houseNumberId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($houseNumberId['max'])) {
                $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_ADHSNB_ID, $houseNumberId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_ADHSNB_ID, $houseNumberId, $comparison);
    }

    /**
     * Filter the query on the cacads_is_urgent column
     *
     * Example usage:
     * <code>
     * $query->filterByIsUrgent(true); // WHERE cacads_is_urgent = true
     * $query->filterByIsUrgent('yes'); // WHERE cacads_is_urgent = true
     * </code>
     *
     * @param     boolean|string $isUrgent The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByIsUrgent($isUrgent = null, $comparison = null)
    {
        if (is_string($isUrgent)) {
            $isUrgent = in_array(strtolower($isUrgent), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_IS_URGENT, $isUrgent, $comparison);
    }

    /**
     * Filter the query on the cacads_is_valid column
     *
     * Example usage:
     * <code>
     * $query->filterByIsValid(true); // WHERE cacads_is_valid = true
     * $query->filterByIsValid('yes'); // WHERE cacads_is_valid = true
     * </code>
     *
     * @param     boolean|string $isValid The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByIsValid($isValid = null, $comparison = null)
    {
        if (is_string($isValid)) {
            $isValid = in_array(strtolower($isValid), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_IS_VALID, $isValid, $comparison);
    }

    /**
     * Filter the query on the cacads_is_photo_pack column
     *
     * Example usage:
     * <code>
     * $query->filterByIsPhotoPack(true); // WHERE cacads_is_photo_pack = true
     * $query->filterByIsPhotoPack('yes'); // WHERE cacads_is_photo_pack = true
     * </code>
     *
     * @param     boolean|string $isPhotoPack The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByIsPhotoPack($isPhotoPack = null, $comparison = null)
    {
        if (is_string($isPhotoPack)) {
            $isPhotoPack = in_array(strtolower($isPhotoPack), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_IS_PHOTO_PACK, $isPhotoPack, $comparison);
    }

    /**
     * Filter the query on the cacads_is_repost_pack column
     *
     * Example usage:
     * <code>
     * $query->filterByIsRepostPack(true); // WHERE cacads_is_repost_pack = true
     * $query->filterByIsRepostPack('yes'); // WHERE cacads_is_repost_pack = true
     * </code>
     *
     * @param     boolean|string $isRepostPack The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByIsRepostPack($isRepostPack = null, $comparison = null)
    {
        if (is_string($isRepostPack)) {
            $isRepostPack = in_array(strtolower($isRepostPack), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_IS_REPOST_PACK, $isRepostPack, $comparison);
    }

    /**
     * Filter the query on the cacads_is_top_pack column
     *
     * Example usage:
     * <code>
     * $query->filterByIsTopPack(true); // WHERE cacads_is_top_pack = true
     * $query->filterByIsTopPack('yes'); // WHERE cacads_is_top_pack = true
     * </code>
     *
     * @param     boolean|string $isTopPack The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByIsTopPack($isTopPack = null, $comparison = null)
    {
        if (is_string($isTopPack)) {
            $isTopPack = in_array(strtolower($isTopPack), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_IS_TOP_PACK, $isTopPack, $comparison);
    }

    /**
     * Filter the query on the cacads_repost_limit column
     *
     * Example usage:
     * <code>
     * $query->filterByRepostLimit('2011-03-14'); // WHERE cacads_repost_limit = '2011-03-14'
     * $query->filterByRepostLimit('now'); // WHERE cacads_repost_limit = '2011-03-14'
     * $query->filterByRepostLimit(array('max' => 'yesterday')); // WHERE cacads_repost_limit > '2011-03-13'
     * </code>
     *
     * @param     mixed $repostLimit The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByRepostLimit($repostLimit = null, $comparison = null)
    {
        if (is_array($repostLimit)) {
            $useMinMax = false;
            if (isset($repostLimit['min'])) {
                $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_REPOST_LIMIT, $repostLimit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($repostLimit['max'])) {
                $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_REPOST_LIMIT, $repostLimit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_REPOST_LIMIT, $repostLimit, $comparison);
    }

    /**
     * Filter the query on the cacads_repost_hour column
     *
     * Example usage:
     * <code>
     * $query->filterByRepostHour('2011-03-14'); // WHERE cacads_repost_hour = '2011-03-14'
     * $query->filterByRepostHour('now'); // WHERE cacads_repost_hour = '2011-03-14'
     * $query->filterByRepostHour(array('max' => 'yesterday')); // WHERE cacads_repost_hour > '2011-03-13'
     * </code>
     *
     * @param     mixed $repostHour The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByRepostHour($repostHour = null, $comparison = null)
    {
        if (is_array($repostHour)) {
            $useMinMax = false;
            if (isset($repostHour['min'])) {
                $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_REPOST_HOUR, $repostHour['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($repostHour['max'])) {
                $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_REPOST_HOUR, $repostHour['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_REPOST_HOUR, $repostHour, $comparison);
    }

    /**
     * Filter the query on the cacads_posted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByPostedAt('2011-03-14'); // WHERE cacads_posted_at = '2011-03-14'
     * $query->filterByPostedAt('now'); // WHERE cacads_posted_at = '2011-03-14'
     * $query->filterByPostedAt(array('max' => 'yesterday')); // WHERE cacads_posted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $postedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByPostedAt($postedAt = null, $comparison = null)
    {
        if (is_array($postedAt)) {
            $useMinMax = false;
            if (isset($postedAt['min'])) {
                $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_POSTED_AT, $postedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($postedAt['max'])) {
                $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_POSTED_AT, $postedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_POSTED_AT, $postedAt, $comparison);
    }

    /**
     * Filter the query on the cacads_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE cacads_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE cacads_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE cacads_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the cacads_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE cacads_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE cacads_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE cacads_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildClassifiedAds $classifiedAds Object to remove from the list of results
     *
     * @return $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function prune($classifiedAds = null)
    {
        if ($classifiedAds) {
            $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_ID, $classifiedAds->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the classifiedads_classifieds_ads_cacads table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClassifiedAdsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ClassifiedAdsTableMap::clearInstancePool();
            ClassifiedAdsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClassifiedAdsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ClassifiedAdsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ClassifiedAdsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ClassifiedAdsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ClassifiedAdsTableMap::COL_CACADS_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ClassifiedAdsTableMap::COL_CACADS_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ClassifiedAdsTableMap::COL_CACADS_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ClassifiedAdsTableMap::COL_CACADS_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildClassifiedAdsQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ClassifiedAdsTableMap::COL_CACADS_CREATED_AT);
    }

} // ClassifiedAdsQuery
