<?php

namespace IiMedias\ClassifiedAdsBundle\Model\Map;

use IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds;
use IiMedias\ClassifiedAdsBundle\Model\ClassifiedAdsQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'classifiedads_classifieds_ads_cacads' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ClassifiedAdsTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.ClassifiedAdsBundle.Model.Map.ClassifiedAdsTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'classifiedads_classifieds_ads_cacads';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\ClassifiedAdsBundle\\Model\\ClassifiedAds';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.ClassifiedAdsBundle.Model.ClassifiedAds';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 22;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 22;

    /**
     * the column name for the cacads_id field
     */
    const COL_CACADS_ID = 'classifiedads_classifieds_ads_cacads.cacads_id';

    /**
     * the column name for the cacads_usrusr_id field
     */
    const COL_CACADS_USRUSR_ID = 'classifiedads_classifieds_ads_cacads.cacads_usrusr_id';

    /**
     * the column name for the cacads_cacatg_id field
     */
    const COL_CACADS_CACATG_ID = 'classifiedads_classifieds_ads_cacads.cacads_cacatg_id';

    /**
     * the column name for the cacads_request field
     */
    const COL_CACADS_REQUEST = 'classifiedads_classifieds_ads_cacads.cacads_request';

    /**
     * the column name for the cacads_type field
     */
    const COL_CACADS_TYPE = 'classifiedads_classifieds_ads_cacads.cacads_type';

    /**
     * the column name for the cacads_name field
     */
    const COL_CACADS_NAME = 'classifiedads_classifieds_ads_cacads.cacads_name';

    /**
     * the column name for the cacads_description field
     */
    const COL_CACADS_DESCRIPTION = 'classifiedads_classifieds_ads_cacads.cacads_description';

    /**
     * the column name for the cacads_price field
     */
    const COL_CACADS_PRICE = 'classifiedads_classifieds_ads_cacads.cacads_price';

    /**
     * the column name for the cacads_photo_list field
     */
    const COL_CACADS_PHOTO_LIST = 'classifiedads_classifieds_ads_cacads.cacads_photo_list';

    /**
     * the column name for the cacads_adtown_id field
     */
    const COL_CACADS_ADTOWN_ID = 'classifiedads_classifieds_ads_cacads.cacads_adtown_id';

    /**
     * the column name for the cacads_adstrt_id field
     */
    const COL_CACADS_ADSTRT_ID = 'classifiedads_classifieds_ads_cacads.cacads_adstrt_id';

    /**
     * the column name for the cacads_adhsnb_id field
     */
    const COL_CACADS_ADHSNB_ID = 'classifiedads_classifieds_ads_cacads.cacads_adhsnb_id';

    /**
     * the column name for the cacads_is_urgent field
     */
    const COL_CACADS_IS_URGENT = 'classifiedads_classifieds_ads_cacads.cacads_is_urgent';

    /**
     * the column name for the cacads_is_valid field
     */
    const COL_CACADS_IS_VALID = 'classifiedads_classifieds_ads_cacads.cacads_is_valid';

    /**
     * the column name for the cacads_is_photo_pack field
     */
    const COL_CACADS_IS_PHOTO_PACK = 'classifiedads_classifieds_ads_cacads.cacads_is_photo_pack';

    /**
     * the column name for the cacads_is_repost_pack field
     */
    const COL_CACADS_IS_REPOST_PACK = 'classifiedads_classifieds_ads_cacads.cacads_is_repost_pack';

    /**
     * the column name for the cacads_is_top_pack field
     */
    const COL_CACADS_IS_TOP_PACK = 'classifiedads_classifieds_ads_cacads.cacads_is_top_pack';

    /**
     * the column name for the cacads_repost_limit field
     */
    const COL_CACADS_REPOST_LIMIT = 'classifiedads_classifieds_ads_cacads.cacads_repost_limit';

    /**
     * the column name for the cacads_repost_hour field
     */
    const COL_CACADS_REPOST_HOUR = 'classifiedads_classifieds_ads_cacads.cacads_repost_hour';

    /**
     * the column name for the cacads_posted_at field
     */
    const COL_CACADS_POSTED_AT = 'classifiedads_classifieds_ads_cacads.cacads_posted_at';

    /**
     * the column name for the cacads_created_at field
     */
    const COL_CACADS_CREATED_AT = 'classifiedads_classifieds_ads_cacads.cacads_created_at';

    /**
     * the column name for the cacads_updated_at field
     */
    const COL_CACADS_UPDATED_AT = 'classifiedads_classifieds_ads_cacads.cacads_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /** The enumerated values for the cacads_request field */

    /** The enumerated values for the cacads_type field */
    const COL_CACADS_TYPE_INDIVIDUAL = 'individual';
    const COL_CACADS_TYPE_PROFESSIONAL = 'professional';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'UserId', 'CategoryId', 'Request', 'Type', 'Name', 'Description', 'Price', 'PhotoList', 'TownId', 'StreetId', 'HouseNumberId', 'IsUrgent', 'IsValid', 'IsPhotoPack', 'IsRepostPack', 'IsTopPack', 'RepostLimit', 'RepostHour', 'PostedAt', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'userId', 'categoryId', 'request', 'type', 'name', 'description', 'price', 'photoList', 'townId', 'streetId', 'houseNumberId', 'isUrgent', 'isValid', 'isPhotoPack', 'isRepostPack', 'isTopPack', 'repostLimit', 'repostHour', 'postedAt', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ClassifiedAdsTableMap::COL_CACADS_ID, ClassifiedAdsTableMap::COL_CACADS_USRUSR_ID, ClassifiedAdsTableMap::COL_CACADS_CACATG_ID, ClassifiedAdsTableMap::COL_CACADS_REQUEST, ClassifiedAdsTableMap::COL_CACADS_TYPE, ClassifiedAdsTableMap::COL_CACADS_NAME, ClassifiedAdsTableMap::COL_CACADS_DESCRIPTION, ClassifiedAdsTableMap::COL_CACADS_PRICE, ClassifiedAdsTableMap::COL_CACADS_PHOTO_LIST, ClassifiedAdsTableMap::COL_CACADS_ADTOWN_ID, ClassifiedAdsTableMap::COL_CACADS_ADSTRT_ID, ClassifiedAdsTableMap::COL_CACADS_ADHSNB_ID, ClassifiedAdsTableMap::COL_CACADS_IS_URGENT, ClassifiedAdsTableMap::COL_CACADS_IS_VALID, ClassifiedAdsTableMap::COL_CACADS_IS_PHOTO_PACK, ClassifiedAdsTableMap::COL_CACADS_IS_REPOST_PACK, ClassifiedAdsTableMap::COL_CACADS_IS_TOP_PACK, ClassifiedAdsTableMap::COL_CACADS_REPOST_LIMIT, ClassifiedAdsTableMap::COL_CACADS_REPOST_HOUR, ClassifiedAdsTableMap::COL_CACADS_POSTED_AT, ClassifiedAdsTableMap::COL_CACADS_CREATED_AT, ClassifiedAdsTableMap::COL_CACADS_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('cacads_id', 'cacads_usrusr_id', 'cacads_cacatg_id', 'cacads_request', 'cacads_type', 'cacads_name', 'cacads_description', 'cacads_price', 'cacads_photo_list', 'cacads_adtown_id', 'cacads_adstrt_id', 'cacads_adhsnb_id', 'cacads_is_urgent', 'cacads_is_valid', 'cacads_is_photo_pack', 'cacads_is_repost_pack', 'cacads_is_top_pack', 'cacads_repost_limit', 'cacads_repost_hour', 'cacads_posted_at', 'cacads_created_at', 'cacads_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'UserId' => 1, 'CategoryId' => 2, 'Request' => 3, 'Type' => 4, 'Name' => 5, 'Description' => 6, 'Price' => 7, 'PhotoList' => 8, 'TownId' => 9, 'StreetId' => 10, 'HouseNumberId' => 11, 'IsUrgent' => 12, 'IsValid' => 13, 'IsPhotoPack' => 14, 'IsRepostPack' => 15, 'IsTopPack' => 16, 'RepostLimit' => 17, 'RepostHour' => 18, 'PostedAt' => 19, 'CreatedAt' => 20, 'UpdatedAt' => 21, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'userId' => 1, 'categoryId' => 2, 'request' => 3, 'type' => 4, 'name' => 5, 'description' => 6, 'price' => 7, 'photoList' => 8, 'townId' => 9, 'streetId' => 10, 'houseNumberId' => 11, 'isUrgent' => 12, 'isValid' => 13, 'isPhotoPack' => 14, 'isRepostPack' => 15, 'isTopPack' => 16, 'repostLimit' => 17, 'repostHour' => 18, 'postedAt' => 19, 'createdAt' => 20, 'updatedAt' => 21, ),
        self::TYPE_COLNAME       => array(ClassifiedAdsTableMap::COL_CACADS_ID => 0, ClassifiedAdsTableMap::COL_CACADS_USRUSR_ID => 1, ClassifiedAdsTableMap::COL_CACADS_CACATG_ID => 2, ClassifiedAdsTableMap::COL_CACADS_REQUEST => 3, ClassifiedAdsTableMap::COL_CACADS_TYPE => 4, ClassifiedAdsTableMap::COL_CACADS_NAME => 5, ClassifiedAdsTableMap::COL_CACADS_DESCRIPTION => 6, ClassifiedAdsTableMap::COL_CACADS_PRICE => 7, ClassifiedAdsTableMap::COL_CACADS_PHOTO_LIST => 8, ClassifiedAdsTableMap::COL_CACADS_ADTOWN_ID => 9, ClassifiedAdsTableMap::COL_CACADS_ADSTRT_ID => 10, ClassifiedAdsTableMap::COL_CACADS_ADHSNB_ID => 11, ClassifiedAdsTableMap::COL_CACADS_IS_URGENT => 12, ClassifiedAdsTableMap::COL_CACADS_IS_VALID => 13, ClassifiedAdsTableMap::COL_CACADS_IS_PHOTO_PACK => 14, ClassifiedAdsTableMap::COL_CACADS_IS_REPOST_PACK => 15, ClassifiedAdsTableMap::COL_CACADS_IS_TOP_PACK => 16, ClassifiedAdsTableMap::COL_CACADS_REPOST_LIMIT => 17, ClassifiedAdsTableMap::COL_CACADS_REPOST_HOUR => 18, ClassifiedAdsTableMap::COL_CACADS_POSTED_AT => 19, ClassifiedAdsTableMap::COL_CACADS_CREATED_AT => 20, ClassifiedAdsTableMap::COL_CACADS_UPDATED_AT => 21, ),
        self::TYPE_FIELDNAME     => array('cacads_id' => 0, 'cacads_usrusr_id' => 1, 'cacads_cacatg_id' => 2, 'cacads_request' => 3, 'cacads_type' => 4, 'cacads_name' => 5, 'cacads_description' => 6, 'cacads_price' => 7, 'cacads_photo_list' => 8, 'cacads_adtown_id' => 9, 'cacads_adstrt_id' => 10, 'cacads_adhsnb_id' => 11, 'cacads_is_urgent' => 12, 'cacads_is_valid' => 13, 'cacads_is_photo_pack' => 14, 'cacads_is_repost_pack' => 15, 'cacads_is_top_pack' => 16, 'cacads_repost_limit' => 17, 'cacads_repost_hour' => 18, 'cacads_posted_at' => 19, 'cacads_created_at' => 20, 'cacads_updated_at' => 21, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
                ClassifiedAdsTableMap::COL_CACADS_REQUEST => array(
                        ),
                ClassifiedAdsTableMap::COL_CACADS_TYPE => array(
                            self::COL_CACADS_TYPE_INDIVIDUAL,
            self::COL_CACADS_TYPE_PROFESSIONAL,
        ),
    );

    /**
     * Gets the list of values for all ENUM and SET columns
     * @return array
     */
    public static function getValueSets()
    {
      return static::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM or SET column
     * @param string $colname
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = self::getValueSets();

        return $valueSets[$colname];
    }

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('classifiedads_classifieds_ads_cacads');
        $this->setPhpName('ClassifiedAds');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\ClassifiedAdsBundle\\Model\\ClassifiedAds');
        $this->setPackage('src.IiMedias.ClassifiedAdsBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('cacads_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('cacads_usrusr_id', 'UserId', 'INTEGER', true, null, null);
        $this->addColumn('cacads_cacatg_id', 'CategoryId', 'INTEGER', true, null, null);
        $this->addColumn('cacads_request', 'Request', 'ENUM', true, null, null);
        $this->getColumn('cacads_request')->setValueSet(array (
));
        $this->addColumn('cacads_type', 'Type', 'ENUM', true, null, null);
        $this->getColumn('cacads_type')->setValueSet(array (
  0 => 'individual',
  1 => 'professional',
));
        $this->addColumn('cacads_name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('cacads_description', 'Description', 'LONGVARCHAR', true, null, null);
        $this->addColumn('cacads_price', 'Price', 'FLOAT', true, null, null);
        $this->addColumn('cacads_photo_list', 'PhotoList', 'ARRAY', true, null, null);
        $this->addColumn('cacads_adtown_id', 'TownId', 'VARCHAR', true, 255, null);
        $this->addColumn('cacads_adstrt_id', 'StreetId', 'VARCHAR', true, 255, null);
        $this->addColumn('cacads_adhsnb_id', 'HouseNumberId', 'INTEGER', true, null, null);
        $this->addColumn('cacads_is_urgent', 'IsUrgent', 'BOOLEAN', true, 1, null);
        $this->addColumn('cacads_is_valid', 'IsValid', 'BOOLEAN', true, 1, null);
        $this->addColumn('cacads_is_photo_pack', 'IsPhotoPack', 'BOOLEAN', true, 1, null);
        $this->addColumn('cacads_is_repost_pack', 'IsRepostPack', 'BOOLEAN', true, 1, null);
        $this->addColumn('cacads_is_top_pack', 'IsTopPack', 'BOOLEAN', true, 1, null);
        $this->addColumn('cacads_repost_limit', 'RepostLimit', 'TIMESTAMP', true, null, null);
        $this->addColumn('cacads_repost_hour', 'RepostHour', 'TIMESTAMP', true, null, null);
        $this->addColumn('cacads_posted_at', 'PostedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('cacads_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('cacads_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'cacads_created_at', 'update_column' => 'cacads_updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ClassifiedAdsTableMap::CLASS_DEFAULT : ClassifiedAdsTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ClassifiedAds object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ClassifiedAdsTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ClassifiedAdsTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ClassifiedAdsTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ClassifiedAdsTableMap::OM_CLASS;
            /** @var ClassifiedAds $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ClassifiedAdsTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ClassifiedAdsTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ClassifiedAdsTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ClassifiedAds $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ClassifiedAdsTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_ID);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_USRUSR_ID);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_CACATG_ID);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_REQUEST);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_TYPE);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_NAME);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_DESCRIPTION);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_PRICE);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_PHOTO_LIST);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_ADTOWN_ID);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_ADSTRT_ID);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_ADHSNB_ID);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_IS_URGENT);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_IS_VALID);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_IS_PHOTO_PACK);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_IS_REPOST_PACK);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_IS_TOP_PACK);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_REPOST_LIMIT);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_REPOST_HOUR);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_POSTED_AT);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_CREATED_AT);
            $criteria->addSelectColumn(ClassifiedAdsTableMap::COL_CACADS_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.cacads_id');
            $criteria->addSelectColumn($alias . '.cacads_usrusr_id');
            $criteria->addSelectColumn($alias . '.cacads_cacatg_id');
            $criteria->addSelectColumn($alias . '.cacads_request');
            $criteria->addSelectColumn($alias . '.cacads_type');
            $criteria->addSelectColumn($alias . '.cacads_name');
            $criteria->addSelectColumn($alias . '.cacads_description');
            $criteria->addSelectColumn($alias . '.cacads_price');
            $criteria->addSelectColumn($alias . '.cacads_photo_list');
            $criteria->addSelectColumn($alias . '.cacads_adtown_id');
            $criteria->addSelectColumn($alias . '.cacads_adstrt_id');
            $criteria->addSelectColumn($alias . '.cacads_adhsnb_id');
            $criteria->addSelectColumn($alias . '.cacads_is_urgent');
            $criteria->addSelectColumn($alias . '.cacads_is_valid');
            $criteria->addSelectColumn($alias . '.cacads_is_photo_pack');
            $criteria->addSelectColumn($alias . '.cacads_is_repost_pack');
            $criteria->addSelectColumn($alias . '.cacads_is_top_pack');
            $criteria->addSelectColumn($alias . '.cacads_repost_limit');
            $criteria->addSelectColumn($alias . '.cacads_repost_hour');
            $criteria->addSelectColumn($alias . '.cacads_posted_at');
            $criteria->addSelectColumn($alias . '.cacads_created_at');
            $criteria->addSelectColumn($alias . '.cacads_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ClassifiedAdsTableMap::DATABASE_NAME)->getTable(ClassifiedAdsTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ClassifiedAdsTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ClassifiedAdsTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ClassifiedAdsTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ClassifiedAds or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ClassifiedAds object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClassifiedAdsTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\ClassifiedAdsBundle\Model\ClassifiedAds) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ClassifiedAdsTableMap::DATABASE_NAME);
            $criteria->add(ClassifiedAdsTableMap::COL_CACADS_ID, (array) $values, Criteria::IN);
        }

        $query = ClassifiedAdsQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ClassifiedAdsTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ClassifiedAdsTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the classifiedads_classifieds_ads_cacads table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ClassifiedAdsQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ClassifiedAds or Criteria object.
     *
     * @param mixed               $criteria Criteria or ClassifiedAds object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClassifiedAdsTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ClassifiedAds object
        }

        if ($criteria->containsKey(ClassifiedAdsTableMap::COL_CACADS_ID) && $criteria->keyContainsValue(ClassifiedAdsTableMap::COL_CACADS_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ClassifiedAdsTableMap::COL_CACADS_ID.')');
        }


        // Set the correct dbName
        $query = ClassifiedAdsQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ClassifiedAdsTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ClassifiedAdsTableMap::buildTableMap();
